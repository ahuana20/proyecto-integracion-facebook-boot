from flask import Blueprint

from controllers.FootBallController import FootBallController
football = FootBallController()

# Registrando Blueprint
footballBP = Blueprint('footballBP', __name__)

@footballBP.route('/list-countries')
def listCountries():
    return football.listCountries()
