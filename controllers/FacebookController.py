import json
from flask import request

from utils.logger import LogInfo
from utils.environment import Environment

from controllers.FootBallController import FootBallController
from controllers.FacebookActionController import FacebookActionController

class FacebookController():

    def __init__(self):
        # Instanciando Clases
        self.actionFootball = FootBallController()
        self.actionFacebook = FacebookActionController()
        self.env = Environment()

    def log(self, data):
        LogInfo('Clase FacebookController', data)

    def validate(self):
        # Obtener las variables de entorno para facebook.
        envFacebook = self.env.facebook()

        # Almacenamos los valores enviados como parametros en el request
        valueHubMode = request.args.get("hub.mode")
        valueHubChallenge = request.args.get("hub.challenge")
        valueHubToken = request.args.get("hub.verify_token")

        if valueHubMode == "subscribe" and valueHubChallenge:
            # Validar que el Token enviando por facebook sea idéntico al que
            # tenemos en nuestro servidor. Si es invalido devolvemos un status
            # code 403. Caso contrario devolvemos un status code 200 con el
            # valor hub.challenge
            if not valueHubToken == envFacebook['TOKEN']:
                return "Verification token mismatch", 403

            return valueHubChallenge, 200

    def receivedMessage(self):
        data = request.get_json()

        # Validar Type Message
        if data["object"] == "page":
            for entry in data["entry"]:
                for messagingEvent in entry["messaging"]:
                    #Captutar el recipient ID
                    recipientID = messagingEvent["sender"]["id"]

                
                    if messagingEvent.get("message"):
                        self.actionFacebook.sendAction(recipientID, 'mark_seen')
                        self.actionFacebook.sendAction(recipientID, 'typing_on')

                        message_text = messagingEvent["message"]["text"]
                        self.log(f'El usuario nos a mandado un mensaje : {message_text}')

                    # Evento cuando usuario hace click en botones
                    if messagingEvent.get("postback"):
                        self.actionFacebook.sendAction(recipientID, 'mark_seen')
                        self.actionFacebook.sendAction(recipientID, 'typing_on')
                        self.controlPostBack(recipientID, messagingEvent.get("postback"))
                    # Evento para recibir delivery

                    if messagingEvent.get("delivery"):
                        self.controlPostBack(recipientID, messagingEvent.get("typing off"))

        return 'OK', 200

    def controlPostBack(self, recipientID, data):
        payload = data['payload'].upper()
        title = data['title'].lower()

        if payload == 'LIST_COUNTRIES':
            self.responseListCountries(recipientID)

        if payload == 'LIST_LEAGUES':
            self.responseListLeagues(recipientID, title)


    def responseListCountries(self, recipientID):
        buttons = []
        for country in self.actionFootball.listCountries():
            buttons.append(
                {
                    "type": "postback",
                    "title": country['country'],
                    "payload": "LIST_LEAGUES"
                }
            )

        templateData = {
            "recipientID": recipientID,
            "text": "Click en el País, para ver las ligas",
            "buttons": buttons
        }
        return self.actionFacebook.sendPostbackButton(templateData)

    def responseListLeagues(self, recipientID, country):
        buttons = []
        for league in self.actionFootball.listLeagues(country):
            buttons.append(
                {
                    "content_type": "text",
                    "title": league['name'],
                    "payload": "LIST_TEAMS",
                    "image_url":league["logo"]
                }
            )
        if len(buttons):
            break

        templateData = {
            "recipientID": recipientID,
            "text": "Click en el País, para ver las ligas",
            "quickReplies": buttons
        }

       

        return self.actionFacebook.sendQuickReplies(templateData)




