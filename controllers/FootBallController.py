import json

from utils.sendRequests import SendRequests
from utils.logger import LogInfo


class FootBallController(SendRequests):

    def listCountries(self):
        allowedCountries =  self.footBall()['FILTERS']['ALLOWED_COUNTRIES']

        dataCountries = self.get('football', 'countries')
        countries = json.loads(dataCountries)['api']['countries']

        listCountries = []
        
        for country in countries:
            # Filtrar los países que están permitidos para mostrar
            if country['country'] in allowedCountries:
                 listCountries.append(country)

        return listCountries

    def listLeagues(self, country):
        season = self.footBall()['FILTERS']['SEASON']

        endpoint =  f'leagues/country/{country.lower()}/{season}'
        print(endpoint)

        dataLeagues = self.get('football', endpoint)
        leagues = json.loads(dataLeagues)['api']['leagues']

        listLeagues = []
        for league in leagues:
            listLeagues.append(league)

        return listLeagues

    def listTeams(self):
        pass

    def listPlayers(self):
        pass

    def listDetailPlater(self):
        pass

    def log(self, data):
        LogInfo('Clase FootBallController', data)
