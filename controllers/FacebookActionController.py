
from utils.sendRequests import SendRequests

# Clase que administra las acciones que se envían a Facebook
class FacebookActionController(SendRequests):

    def sendMessage(self, recipientID, messageText):
        data = {
            "recipient": {
                "id": recipientID
            },
            "message": {
                "text": messageText
            }
        }

        # Mostrar el efecto de escribiendo hacia el facebook
        self.sendAction(recipientID, 'typing_on')

        # Enviando el mensaje hacia facebook
        return self.post('facebook', 'me/messages', data)

    def sendAction(self, recipientID, senderAction):
        # senderAction solo soporte tres formas
        # mark_seen  : Para marcar como leído el ultimo mensaje
        # typing_on  : Para mostrar el efecto de escribir
        # typing_off : Para retirar el efecto de escribir
        data = {
            "recipient": {
                "id": recipientID
            },
            "sender_action": senderAction
        }

        self.post('facebook', 'me/messages', data)


    def sendPostbackButton(self, data):
        templateData = {
            "recipient": {"id": data['recipientID']},
            "message": {
                "attachment": {
                    "type": "template",
                    "payload": {
                        "template_type":"button",
                        "text":  data['text'],
                        "buttons": data['buttons']
                    }
                }
            }
        }

        return self.post('facebook', 'me/messages', templateData)


    def sendQuickReplies(self, data):
        templateData = {
            "recipient": {"id": data['recipientID']},
            "messaging_type": "RESPONSE",
            "message": {
                "text": data['text'],
                "quick_replies": data['quickReplies']
            }
        }

        print(templateData)

        return self.post('facebook', 'me/messages', templateData)


