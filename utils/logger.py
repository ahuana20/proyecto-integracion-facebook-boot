import json
from colorama import init
from termcolor import colored

def LogInfo(message, data):
    print(message)
    print(json.dumps(data, indent=2))

def LogError(message, data):
    print(message)
    print(json.dumps(data,indent=2))

def LogDebug(message, data):
    pass



# use Colorama to make Termcolor work on Windows too
init()

# then use Termcolor for all colored text output
print(colored('Hello, World!', 'green', 'on_red'))